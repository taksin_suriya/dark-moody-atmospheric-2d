using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Movement Parameters
    [Header("Movement Parameters")]
    [SerializeField] private float playerSpeed = 5f;
    private bool isFacingRight = true;

    // Jump Parameters
    [Header("Jump Parameters")]
    [SerializeField] private float jumpForce = 10f;
    [SerializeField] private int jumpAmount = 1;
    private int jumpCounter = 0;

    // Ground Check
    [Header("Ground Check")]
    [SerializeField] private float groundCheckRadius = 0.1f;
    [SerializeField] private Transform groundCheckPos;
    [SerializeField] private LayerMask groundLayer;
    private bool isGrounded;

    // Dash Parameters
    [Header("Dash Parameters")]
    [SerializeField] private float dashForce;
    [SerializeField] private float waitTimeDash = .2f;
    [SerializeField] private int dashAmount = 1;
    private int dashCounter = 0;
    private bool canDash, isDashing;
    private float dashDir;

    // Wall Jump Parameters
    [Header("Wall Jump")]
    [SerializeField] private float wallJumpGravity;
    [SerializeField] private Transform wallJumpCheckpos;
    [SerializeField] private float wallJumpForceX = 10f;
    [SerializeField] private float wallJumpForceY = 10f;
    private bool canGrab;
    private bool isGrabbing;
    [SerializeField] private float wallJumpRadius = .1f;
    float startGravity;
    [SerializeField] private float startWallJumpTimmer;
    float wallJumpTimer;

    private Rigidbody2D rb;
    private Animator playerAnimator;
    private TrailRenderer trailRenderer;
    private float scaleX;

    // Start is called before the first frame update
    void Start()
    {
        trailRenderer = GetComponent<TrailRenderer>();
        rb = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        jumpCounter = jumpAmount;
        dashCounter = dashAmount;
        canDash = true;
        trailRenderer.emitting = false;
        startGravity = rb.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        rb.gravityScale = isGrabbing ? wallJumpGravity : startGravity;
        CheckGrounded();
        Move();
        Jump();

        if (Input.GetKeyDown(KeyCode.LeftShift) && canDash && dashCounter > 0)
        {
            dashDir = Input.GetAxisRaw("Horizontal");
            dashCounter--;
            isDashing = true;
            canDash = false;
        }

        if (isDashing)
        {
            trailRenderer.emitting = true;
            if (dashDir == 0)
            {
                dashDir = transform.localScale.x;
            }

            rb.velocity = new Vector2(dashDir * dashForce, rb.velocity.y) * Time.deltaTime;
            StartCoroutine(stopDash());
        }
    }

    void CheckGrounded()
    {
        if (isGrounded)
        {
            jumpCounter = jumpAmount;
            dashCounter = dashAmount;
            playerAnimator.SetBool("hasJumped", !isGrounded);
        }
    }

    void Move()
    {
        if (wallJumpTimer <= 0)
        {

        }
        else
        {
            wallJumpTimer -= Time.deltaTime;
        }
        isGrounded = Physics2D.OverlapCircle(groundCheckPos.position, groundCheckRadius, groundLayer);
        float xInput = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(xInput * playerSpeed * Time.deltaTime, rb.velocity.y);

        if ((xInput > 0 && !isFacingRight) || (xInput < 0 && isFacingRight))
        {
            Flip();
        }

        if (xInput != 0)
        {
            playerAnimator.SetBool("isWalking", true);
        }
        else
        {
            playerAnimator.SetBool("isWalking", false);
        }

        canGrab = Physics2D.OverlapCircle(wallJumpCheckpos.position, wallJumpRadius, groundLayer);
        isGrabbing = false;

        if (!isGrounded && canGrab)
        {
            scaleX = transform.localScale.x;
            if (scaleX > 0 && xInput > 0 || scaleX < 0 && xInput < 0)
            {
                isGrabbing = true;
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    wallJumpTimer = startWallJumpTimmer;
                    rb.velocity = new Vector2(-xInput * wallJumpForceX, wallJumpForceY);
                    rb.gravityScale = startGravity;
                    isGrabbing = false;
                }
            }
        }

        if (isGrabbing)
        {
            rb.gravityScale = wallJumpGravity;
            rb.velocity = Vector2.zero;
        }
        else
        {
            rb.gravityScale = startGravity;
        }
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && jumpCounter > 0)
        {
            rb.velocity = Vector2.up * jumpForce;
            playerAnimator.SetBool("hasJumped", true);

            if (!isGrounded)
            {
                jumpCounter--;
            }
        }
    }

    void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 localScale = transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }

    IEnumerator stopDash()
    {
        yield return new WaitForSeconds(waitTimeDash);
        canDash = true;
        isDashing = false;
        trailRenderer.emitting = false;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = isGrounded ? Color.red : Color.green;
        Gizmos.DrawSphere(groundCheckPos.position, groundCheckRadius);
        Gizmos.DrawSphere(wallJumpCheckpos.position, wallJumpRadius);
    }
}
